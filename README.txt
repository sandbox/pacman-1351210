opensimsso2.module - Instructions
--------------------------------------
1. Place the opensimsso folder in your desired modules path

2. Install through the /admin/modules method

3. Configure your Remote Open Sim Database Details by going to
   /admin/opensim
   
4. As at 24/11/2011 Only the Mssql Driver has been provided. 
   Implement new Drivers by following the how the Mssql Driver behaves.
   All Drivers must extend the OpenSimAccess class.
   
5. You can try the Demo by going to
   /opensim/sso

6. Then you can place the [Open Sim Virtual World login button] block in any region of your site
   to generate the Open Sim Login Link.