<?php
/**
 * @file
 * opensim.user.inc
 * - The User Object that SETS/UPDATES both local and remote Open Sim Account information
 */
class OpenSimUser  {
	
	public $driverClass;
	
	/**
	 * LOCAL OpenSim ACCOUNT
	 */
	public $uid;  	     
	public $opensimUuid;  

	/**
	 * REMOTE OpenSim ACCOUNT
	 */
	public $UUID;
	public $firstName;
	public $lastName;
	public $password;
	public $loginKey;
	public $hmRegion  = '';
	public $ownerName = '';
	public $nx_UUID	  = '';
	
	/**
	 * Constructor
	 */
	function __construct($uid,$driverClass) {
		
	  	$this->driverClass = $driverClass;
		$this->setLocalOpenSimUser($uid);
		$this->setOpenSimUser();
		$this->setLoginKey();
		
	}
	
	/**
	 * SET the User Fields before Creating a New Remote Open Sim User
	 */
	function setUserFields($firstName, $lastName, $password, $hmRegion) {
		
		$this->UUID = $this->makeRandomGuid();
		
		if ( !$this->isGUID($this->UUID) ) {
			return "Invalid UUID";
		}
		if ( !$this->isAlphabetNumericSpecial($firstName) ) {
			return "Invalid first name";
		}
		if ( !$this->isAlphabetNumericSpecial($lastName) ) {
			return "Invalid last name";
		}
		if ( !$this->isAlphabetNumericSpecial($password) ) {
			return "Invalid password";
		}
		if ( strlen($password) < AVATAR_PASSWD_MINLEN ) {
			return "Invalid password to short";
		}
		
		$this->firstName = $firstName;
		$this->lastName  = $lastName;
		$this->password	 = $password;
		$this->hmRegion  = $hmRegion;
		
	  	return "";

	}
	
	/**
	 * SET the Local Open Sim User Fields
	 */
	function setLocalOpenSimUser($uid) {
		$this->uid         = $uid;
		$result			   = db_query('SELECT opensim_uuid FROM {opensim_users}  WHERE uid = :uid', array(':uid' => $uid));
		if($result) {
			$record 	   = $result->fetchObject();
			if( is_object($record) ) {
				$this->opensimUuid = $record->opensim_uuid;
			}
		}
	}
	
	/**
	 * SET User Name details from Remote Open Sim Account
	 */
	function setOpenSimUser() {
		$driverClass = $this->driverClass;
		
		if( !empty($this->opensimUuid) ) {
			$fields = array("PrincipalID" => "'".$this->opensimUuid."'");
			
			$row = $driverClass::getOpenSimUserData($fields);
				
			$this->firstName = $row["firstName"];
			$this->lastName  = $row["lastName"];
		}
		
	}
	
	/**
	 * Create/SET the Web Login Key
	 */
	function setLoginKey() {
		$this->loginKey = $this->makeRandomGuid();
	}
	
	/** 
	 * INSERT a local Open Sim User Account
	 */
	function insertLocalOpenSimUser($fields) {
		$result = db_insert('opensim_users')->fields($fields)->execute();
		$this->opensimUuid = $fields["opensim_uuid"];
		return $result;
	}
	
	/**
	 * UPDATE Remote Open Sim User Auth
	 */
	function updateUserAuth() {
		$driverClass = $this->driverClass;
		$setFields["webLoginKey"] = "'".$this->loginKey   ."'";
		$updateFields["UUID"]     = "'".$this->opensimUuid."'";
		$driverClass::updateUserAuth($setFields,$updateFields);
	}
	
	/**
     * Find out if User has a Remote Open Sim Account
	 */
	function hasRemoteOpenSimAccount($firstName,$lastName) {
		$driverClass = $this->driverClass;
		
		$fields["FirstName"] = "'".$driverClass::sqlEscape($firstName)."'";
		$fields["LastName"]  = "'".$driverClass::sqlEscape($lastName)."'";
		
		$result = $driverClass::getPrincipalIDGivenName($fields);
		
		if($result) {
			
			while($row = mssql_fetch_array($result)) {

				$fields = array ("uid" 		    => $this->uid,
								 "opensim_uuid" =>  mssql_guid_string($row['PrincipalID'])
				);
				/**
				 * IF there is a Remote Open Sim Account Insert Local Open Sim Account.
				 */
				$this->insertLocalOpenSimUser($fields);
				return true;
			
			}
			
		}
		
		return false;
	}
	
	/**
	 * Create a New Remote Open Sim User Account
	 */
	function createOpenSimAvatar($region, $homePosition, $homePosition, $homeLookAt, $lastPosition, $lastLookAt) {
		
		$driverClass = $this->driverClass;
		
		$this->UUID  = $this->makeRandomGuid();
		
		$nulluuid    = "00000000-0000-0000-0000-000000000000";
		$passwdsalt  = $this->makeRandomHash();
		$passwdhash  = md5( md5($this->password).":".$passwdsalt );
	
		$serviceURLs = "HomeURI= GatekeeperURI= InventoryServerURI= AssetServerURI=";

		$fields               = array();
		$fields["regionName"] = $region; 
		$regionID 			  = $driverClass::getUserRegionId($fields);
	
		$fields = array("PrincipalID"   => "'".$this->UUID."'",
						"ScopeID" 	    => "'".$nulluuid."'",
						"FirstName"     => "'".$this->firstName."'",
						"LastName"	    => "'".$this->lastName."'",
						"Email" 	    => "''",
						"ServiceURLs"   => "'".$serviceURLs."'",
						"Created" 	    => "'".time()."'",
						"UserLevel"     => "'0'",
						"UserFlags"     => "'0'",
						"UserTitle"     => "'0'",
						);
		$driverClass::insertOpenSimAvatar($fields);
		
		$fields = array("UserID" 		=> "'".$this->UUID."'",
						"HomeRegionID"  => "'".$regionID."'",
						"HomePosition"  => "'".$homePostion."'",
						"HomeLookAt" 	=> "'".$homeLookAt."'",
						"LastRegionID"  => "'".$regionID."'",
						"LastPosition"  => "'".$lastPosition."'",
						"LastLookAt" 	=> "'".$lastLookAt."'",
						"Online" 		=> "'false'",
						"Login" 		=> "'0'",
						"Logout" 		=> "'0'",
						);
		$driverClass::insertGridUser($fields);
	
		$fields = array("UUID" 		    => "'".$this->UUID."'",
						"passwordHash"  => "'".$passwdhash."'",
						"passwordSalt"  => "'".$passwdsalt."'",
						"webLoginKey"   => "'".$nulluuid."'",
						"accountType"   => "'UserAccount'",
						);
		$driverClass::insertUserAuth($fields);
		
		//Lets update the drupal database to recouser->rd the fact the user now has an opensim account.
		$fields = array ('uid' 			=> $this->uid,
						 'opensim_uuid' => $this->UUID);
		$this->insertLocalOpenSimUser($fields);
		
		/**
		 * Create User Inventory Folders
		 * Uncomment below line if you want to execute this
		 */
		#$this->createOpenSimInventoryFolders();
	
		return true;
	}
	
	/**
     * Create Inventory Folders for Open Sim User
     * Note: This is not called at the moment. Test is out as needed.
	 */
	function createOpenSimInventoryFolders() {
		
		$driverClass = $this->driverClass;
	
		$myInventory = $this->makeRandomGuid();
	
		$fields = array('folderName' 	 => 'My Inventory',
						'type'		 	 => 8,
						'version'	 	 => 1,
						'folderID'	 	 => $myInventory,
						'agentID'	 	 => $this->UUID,
						'parentFolderID' => '00000000-0000-0000-0000-000000000000'
					    );
		$driverClass::insertUserInventoryFolder($fields);
		
	
		$fields = array('folderName'     => 'Textures',
						'type'		     => 0,
						'version'		 => 1,
						'folderID'		 => $this->makeRandomGuid(),
						'agentID'		 => $this->UUID,
						'parentFolderID' => $myInventory
					    );
		$driverClass::insertUserInventoryyFolder($fields);
	
		$fields = array('folderName' 	 => 'Sounds',
						'type'			 => 1,
						'version'		 => 1,
						'folderID'		 => $this->makeRandomGuid(),
						'agentID'		 => $this->UUID,
						'parentFolderID' =>$myInventory
						);
		$driverClass::insertUserInventoryFolder($fields);
	
		$fields = array('folderName' 	 => 'Calling Cards',
						'type'			 => 2,
					    'version'		 => 1,
						'folderID'		 => $this->makeRandomGuid(),
						'agentID'		 => $this->UUID,
						'parentFolderID' => $myInventory
						);
		$driverClass::insertUserInventoryFolder($fields);
	
		$fields = array('folderName' 	 => 'Landmarks',
						'type'		 	 => 3,
						'version'		 => 1,
						'folderID'		 => $this->makeRandomGuid(),
						'agentID'		 => $this->UUID,
						'parentFolderID' => $myInventory
					    );
		$driverClass::insertUserInventoryFolder($fields);
	
		$fields = array('folderName' 	 => 'Clothing',
						'type'			 => 5,
						'version'		 => 1,
						'folderID'		 => $this->makeRandomGuid(),
						'agentID'		 => $this->UUID,
						'parentFolderID' => $myInventory
						);
		$driverClass::insertUserInventoryFolder($fields);
	
		$fields = array('folderName' 	 => 'Objects',
						'type'		 	 => 6,
						'version'		 => 1,
						'folderID'		 => $this->makeRandomGuid(),
						'agentID'		 => $this->UUID,
						'parentFolderID' => $myInventory
					    );
		$driverClass::insertUserInventoryFolder($fields);
	
		$fields = array('folderName' 	 => 'Notecards',
						'type'		 	 => 7,
						'version'	 	 => 1,
						'folderID'	 	 => $this->makeRandomGuid(),
						'agentID'	  	 => $this->UUID,
						'parentFolderID' => $myInventory,
					   );
		$driverClass::insertUserInventoryFolder($fields);
	
		$fields = array('folderName' 	 => 'Scripts',
						'type'			 => 10,
						'version'		 => 1,
						'folderID'		 => $this->makeRandomGuid(),
						'agentID'		 => $this->UUID,
						'parentFolderID' => $myInventory,
						);
		$driverClass::insertUserInventoryFolder($fields);
	
		$fields = array('folderName'     => 'Body Parts',
						'type'		     => 13,
						'version'		 => 1,
						'folderID'		 => $this->makeRandomGuid(),
						'agentID'		 => $this->UUID,
						'parentFolderID' => $myInventory,
						);
		$driverClass::insertUserInventoryFolder($fields);
	
		$fields = array('folderName' 	 => 'Trash',
						'type'			 => 14,
						'version'		 => 1,
						'folderID'		 => $this->makeRandomGuid(),
						'agentID'		 => $this->UUID,
						'parentFolderID' => $myInventory,
						);
		$driverClass::insertUserInventoryFolder($fields);
	
		$fields = array('folderName'     => 'Photo Album',
						'type'		     => 15,
						'version'		 => 1,
						'folderID'		 => $this->makeRandomGuid(),
						'agentID'		 => $this->UUID,
						'parentFolderID' => $myInventory,
						);
		$driverClass::insertUserInventoryFolder($fields);
	
		$fields = array('folderName' 	 => 'Lost And Found',
						'type'		 	 => 16,
						'version'	 	 => 1,
						'folderID'	 	 => $this->makeRandomGuid(),
						'agentID'	 	 => $this->UUID,
						'parentFolderID' => $myInventory,
						);
		$driverClass::insertUserInventoryFolder($fields);
	
		$fields = array('folderName' 	 => 'Animations',
						'type'		 	 => 20,
						'version'		 => 1,
						'folderID'		 => $this->makeRandomGuid(),
						'agentID'		 => $this->UUID,
						'parentFolderID' => $myInventory,
						);
		$driverClass::insertUserInventoryFolder($fields);
	
		$fields = array('folderName' 	 => 'Gestures',
						'type'			 => 21,
						'version'		 => 1,
						'folderID'		 => $this->makeRandomGuid(),
						'agentID'		 => $this->UUID,
						'parentFolderID' => $myInventory,
						);
		$driverClass::insertUserInventoryFolder($fields);
	
		return true;
	}
	
	/**
     * Return a Random UUID
	 */
	function makeRandomGuid() {
		$uuid = sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
				mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
				mt_rand( 0, 0x0fff ) | 0x4000,
				mt_rand( 0, 0x3fff ) | 0x8000,
				mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ) );
		return $uuid;
	}
	
	/**
     * Return a Random Hash
	 */
	function makeRandomHash() {
		$ret = sprintf('%04x%04x%04x%04x%04x%04x%04x%04x',mt_rand(0,0xffff),mt_rand(0,0xffff),mt_rand(0,0xffff),mt_rand(0,0xffff),
				mt_rand(0,0xffff),mt_rand(0,0xffff),mt_rand(0,0xffff),mt_rand(0,0xffff));
		return $ret;
	}
	
	/**
	 * Return if a variable has the UUID Format
	 */
	function isGUID($uuid, $nullok=false) {
		if ($uuid==null) return $nullok;
		if (!preg_match('/^[0-9A-Fa-f]{8,8}-[0-9A-Fa-f]{4,4}-[0-9A-Fa-f]{4,4}-[0-9A-Fa-f]{4,4}-[0-9A-Fa-f]{12,12}$/', $uuid)) return false;
		return true;
	}
	
	/**
	 * Return if String is Alpha-Numeric
	 */
	function isAlphabetNumericSpecial($str, $nullok=false) {
		if ($str!='0' and $str==null) return $nullok;
		if (!preg_match('/^[_a-zA-Z0-9 &@%#\-\.]+$/', $str)) return false;
		return true;
	}
	
}