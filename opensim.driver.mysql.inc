<?php
/**
 * @file
 * opensim.db.mysql.inc
 * - MYSQL Database Driver
 * - Extends OpenSimDb Database Driver
 * - Implements openSimAccess Calls
 */
class OpenSimDriverMysql extends OpenSimDb implements openSimAccess {
	
	/**
	 * Contructor
	 */
	public function __construct() {
		/**
		 * Set your Mysql DB Table Names
		 */
		$this->userTable          = "UserAccounts";
		$this->userGridTable      = "GridUser";
		$this->userAuthTable      = "auth";
		$this->userInventoryTable = "inventoryfolders";
		$this->userRegionsTable   = "regions";
		parent::__construct();
	}
	
	/**
	 * The Singleton Function that returns the $instance attribute which holds the only instance of this class.
	 */
	static public function singleton() {
		if(self::$instance == NULL) {
			self::$instance = new OpenSimDriverMysql();
		}
		return self::$instance;
	}
	
	/**
	 * Connect to the remote database
	 */
	static public function connect() { }
	
	/**
	 * Disconnect from the remote database
	 */
	static public function disconnect() { }
	
	/**
	 * The following functions implement the openSimAccess Interface
	 */
	
	static public function connect() { }
	
	static public function disconnect() { }
	
	static public function getOpenSimUserData($fields) { }
	
	static public function insertUserInventoryFolder($fields) { }
	
	static public function updateUserAuth($setFields,$updateFields) { }
	
	static public function getUserRegionId($fields) { }
	
	static public function insertOpenSimAvatar($fields) { }
	
	static public function insertGridUser($fields) { }
	
	static public function insertUserAuth($fields) { }
	
	static public function getPrincipalIDGivenName($fields) { }
	
}