<?php
/**
 * @file
 * opensim.db.inc
 * - OpenSim Database Parent Class 
 * - Inherited by particular database driver classes (e.g. mysql, mssql...)
 */
abstract class OpenSimDb  {
  
	/**
	 * Persistant Singleton Instance
	 */
  	static protected $instance = NULL;

	static protected $serverHost         = "";
	static protected $remoteUsername     = "";
	static protected $remotePassword     = "";
	static protected $remoteDatabase     = "";
  	static protected $userTable          = "";
	static protected $userGridTable      = "";
	static protected $userAuthTable      = "";
	static protected $userInventoryTable = "";
	static protected $userRegionsTable   = "";
	static protected $connectable        = FALSE;
	static protected $connected          = FALSE;
	static protected $errors             = array();
	
	/**
	 * Constructor
	 */
	function __construct() { 
		$this->setOpenSimDb(); 
  	}
  	
	/**
	 * Set your attributes whilst in Constructing Mode 
	 * ("$this" syntax can be used but after constructing you must use "self::$instance->attribute" syntax
	 */
	function setOpenSimDb() {
		
		$this->serverHost     = variable_get("server_host", '');		
		$this->remoteDatabase = variable_get("remote_database",'');
		$this->remoteUsername = variable_get("remote_username", '');
		$this->remotePassword = variable_get("remote_password", '');
		$this->errors 	      = array();
		
		if( !empty($this->serverHost) && 
		    !empty($this->remoteDatabase) && 
		    !empty($this->remoteUsername) &&
		    !empty($this->remotePassword) ) {
			$this->connectable = TRUE;
		}
		
	}
	
	/**
	 * Connect Function must be overriden
	 */
	static abstract protected function connect();
	
	/**
	 * Disconnect Function must be overriden
	 */
	static abstract protected function disconnect();
	
	/**
	 * Return whether or not Driver is connected
	 */
	static function isConnected() {
		return self::$instance->connected;
	}
	
	/**
	 * Return whether or not Driver is connectable
	 */
	static function isConnectable() {
		return self::$instance->connectable;
	}
	
	/**
	 * Return Driver Errors
	 */
	static function getErrors() {
		return self::$instance->errors;
	}

	/**
	 * Return Sql for WHERE Clause given a Field/Value array and an Operator
	 */
	static function whereSql($fields, $operator) {
		$sql = "";
		foreach($fields as $key => $value) {
			$sql .= ((!empty($sql))?" ".$operator." ":"") . $key . " = " . $value . " ";
		}
		return $sql;
	}
	
	/**
	 * Return Sql for SET Clause given a Field/Value array
	 */
	static function setSql($fields) {
		$sql = "";
		foreach($fields as $key => $value) {
			$sql .= ((!empty($sql))?", ":" ") . $key . " = " . $value . " ";
			$i++;
		}
		return $sql;
	}
	
	/**
	 * Return SQL for INSERT statement, return both (name1,name2...) and (value1,value2...) syntax
	 */
	static function nameValueSql($nameValues) {
		$nameValueSql 	 = array();
		$fieldNames      = array();
		$fieldValues     = array();
		foreach($nameValues as $name => $value) {
			$fieldNames[]  = $name;
			$fieldValues[] = $value;
		}
		$nameValueSql["names"]  = implode(",",$fieldNames);
		$nameValueSql["values"] = implode(",",$fieldValues);
		
		return $nameValueSql;
	}
	
	/**
	 * SQL Value Escape function
	 */
	static function sqlEscape($sql) {
	
		/* De MagicQuotes */
		$fix_str = stripslashes($sql);
		$fix_str = str_replace("'","''",$fix_str);
		$fix_str = str_replace("\0","[NULL]",$fix_str);
		return $fix_str;
	}
	
}