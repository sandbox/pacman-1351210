<?php
/**
 * @file
 * opensim.api.php
 * - The Open Sim API that makes class to an Open Sim Connection using a Driver
 */

/**
 * Include Open Sim API Classes
 */
module_load_include('inc', 'opensimsso', 'opensim.user');
module_load_include('inc', 'opensimsso', 'opensim.access');
module_load_include('inc', 'opensimsso', 'opensim.db');

class OpenSimAPI  {

	private $connectionDriver;
	private $driverClass;
	private $user;            //User Object

	/**
	 * Constructor
	 */
	function __construct($uid) {
		$this->setConnectionDriver();
		$this->user = new OpenSimUser($uid,$this->driverClass);
	}
	
	/**
	 * Set the Connection Driver used through out API
	 */
	function setConnectionDriver() {
		
		$connectionDriver = variable_get("connection_driver", '');
		
		if( !empty($connectionDriver) ) {
		
	  		$modulePath = drupal_get_path('module', "opensimsso");
	  
	  		if( file_exists($modulePath."/opensim.driver.".$connectionDriver.".inc") ) {
	  			$this->connectionDriver = $connectionDriver;
	  		
	  			$this->driverClass      = "OpenSimDriver".ucwords($connectionDriver);
	  			
	  			/** Create the first instance of the Open Sim Connection Driver Singleton Class **/
	  			$driverClass 		    = $this->driverClass;
	  			$driverClass::singleton();
	  			$driverClass::connect();
	  			
	  			return TRUE;
	  		}
		
		}
		
		return FALSE;
		
	}
	
	/**
	 * Return Errors if any, collected through out Driver usage
	 */
	function errorsFound() {
		$driverClass = $this->driverClass;
		$errors = $driverClass::getErrors();
		if(sizeof($errors) > 0) {
			return implode(",",$errors);	
		}
		return false;
	} 
	
	/**
	 * Check If this API is connected to Open Sim
	 */
	function isConnected() {
		$driverClass = $this->driverClass;
		return $driverClass::isConnected();
	}
	
	/**
	 * Disconnect API from Open Sim
	 */
	function disconnect() {
		$driverClass = $this->driverClass;
		$driverClass::disconnect();
	}
	
	/**
	 * Get Opem Sim Login URL
	 */
	function getOpenSimLoginUrl() {
					
		$this->user->setLoginKey();
		
		$this->user->updateUserAuth();
		
		$urlProtocol = "secondlife:///";
		$urlScript   = "app/login"; 
		$loginLink   = $urlProtocol.$urlScript."?last_name=".$this->user->lastName."&first_name=".$this->user->firstName."&web_login_key=".$this->user->loginKey."&grid=plane";
		
		return $loginLink;
	
	}
	
	/**
	 * Check if User has Local Open Sim Account
	 */
	function userHasLocalOpenSimAccount() {
		if( !empty($this->user->opensimUuid) ) return TRUE;
	}
		
	/**
	 * Check if User has Remote Open Sim Account
	 */
	function userHasRemoteOpenSimAccount($firstName,$lastName) {
		return $this->user->hasRemoteOpenSimAccount($firstName,$lastName);
	}
	
	/**
	 * Set the User Fields before you Create Open Sim User
	 */
	function setUser($firstName, $lastName, $password, $hmRegion) {
		return $this->user->setUserFields($firstName, $lastName, $password, $hmRegion);
	}
	
	/**
	 * Create a new Open Sim User
	 */
	
	function createOpenSimAvatar($region, $homePosition, $homePosition, $homeLookAt, $lastPosition, $lastLookAt) {
		$this->user->createOpenSimAvatar($region, $homePosition, $homePosition, $homeLookAt, $lastPosition, $lastLookAt);
	}
	
}