<?php
/**
 * @file
 * opensim.db.mssql.inc
 * - MSSQL Database Driver
 * - Extends OpenSimDb Database Driver
 * - Implements openSimAccess Calls
 */
class OpenSimDriverMssql extends OpenSimDb implements openSimAccess {

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->userTable          = "UserAccounts";
		$this->userGridTable      = "GridUser";
		$this->userAuthTable      = "auth";
		$this->userInventoryTable = "inventoryfolders";
		$this->userRegionsTable   = "regions";
		parent::__construct();
	}
	
	/**
	 * The Singleton Function that returns the $instance attribute which holds the only instance of this class.
	 */
	static public function singleton() {
		// Instance must be static in order to be referenced here
		if(self::$instance == NULL) {
			self::$instance = new OpenSimDriverMssql();
		}
		return self::$instance;
	} 
	
	/**
	 * Connect to the remote database
	 */
	static public function connect() {	
		
		if(!self::$instance->isConnectable()) { 
			self::$instance->errors[] = "ERROR: Incorrect Database Details provided";
			return false;
		}
		
		$mssqlLink                 = mssql_connect(self::$instance->serverHost,self::$instance->remoteUsername,self::$instance->remotePassword);
		if(!$mssqlLink) {
			self::$instance->errors[] = "ERROR: Cannot Connect to Remote OpenSim Database with Details provided";
			return false;
		}
		
		$dbSelected                = mssql_select_db(self::$instance->remoteDatabase, $mssqlLink);
		if(!$dbSelected) {
			self::$instance->errors[] = "ERROR: Cannot Select OpenSim Database with Details provided.";
			return FALSE;
		}
		
		self::$instance->connected = TRUE;
		/**
		 * The following functions implement the openSimAccess Interface
		 */
		return true;
	}
	
	/**
	 * Disconnect from the remote database
	 */
	static public function disconnect() {
		if(self::$instance->connected) {
	  		mssql_close();
			self::$instance->connected = FALSE;
		}
	}
	
	/**
	 * The following functions implement the openSimAccess Interface
	 */
	
	static public function getOpenSimUserData($fields) {	
		$sql    = "SELECT * FROM " . self::$instance->userTable . " WHERE " . self::$instance->whereSql($fields,"AND");
		$result = mssql_query($sql);
		if(!$result) self::$instance->errors[] = "ERROR: ".$sql;
		$row    = mssql_fetch_array($result);
		/** keys need to match class attributes **/
		$newRow = array();
		$newRow["firstName"] = $row["FirstName"];
		$newRow["lastName"]  = $row["LastName"];
		mssql_free_result($result);
		return $newRow;
	}
	
	static public function insertUserInventoryFolder($fields) {
		$nameValueSql = self::$instance->nameValueSql($fields);
		$sql 		  = "insert into " . self::$instance->userInventoryTable . " (" . $nameValueSql["names"] . ") values (" . $nameValueSql["values"] . ")";
		$result 	  = mssql_query($sql);
		if(!$result) self::$instance->errors[] = "ERROR: ".$sql;
	}
	
	static public function updateUserAuth($setFields,$updateFields) {
		$sql 		  = "update " . self::$instance->userAuthTable . " set " . self::$instance->setSql($setFields) . " where " . self::$instance->whereSql($updateFields,"AND");
		$result 	  = mssql_query($sql);
		if(!$result) self::$instance->errors[] = "ERROR: ".$sql;
	}
	
	static public function getUserRegionId($fields) {
		$sql 		  = "SELECT uuid FROM " . self::$instance->userRegionsTable . " WHERE " . self::$instance->whereSql($fields,"AND");
		$result 	  = mssql_query($sql);
		if(!$result) self::$instance->errors[] = "ERROR: ".$sql;
		$row      	  = mssql_fetch_array($result);
		$regionID 	  = mssql_guid_string($row['uuid']);
		return $regionID;
	}
	
	static public function insertOpenSimAvatar($fields) {
		$nameValueSql = self::$instance->nameValueSql($fields);
		$sql 		  = "insert into " . self::$instance->userTable . " (" . $nameValueSql["names"] . ")  values (" . $nameValueSql["values"] . ")";
		$result 	  = mssql_query($sql);
		if(!$result) self::$instance->errors[] = "ERROR: ".$sql;
	}
	
	static public function insertGridUser($fields) {
		$nameValueSql = self::$instance->nameValueSql($fields);
		$sql 		  = "insert into " . self::$instance->userGridTable . " (" . $nameValueSql["names"] . ")  values (" . $nameValueSql["values"] . ")";
		$result 	  = mssql_query($sql);
		if(!$result) self::$instance->errors[] = "ERROR: ".$sql;
	}
	
	static public function insertUserAuth($fields) {
		$nameValueSql = self::$instance->nameValueSql($fields);
		$sql 		  = "insert into " . self::$instance->userAuthTable . " (" . $nameValueSql["names"] . ")  values (" . $nameValueSql["values"] . ")";
		$result 	  = mssql_query($sql);
		if(!$result) self::$instance->errors[] = "ERROR: ".$sql;
	}
	
	static public function getPrincipalIDGivenName($fields) {
		$sql = "select PrincipalID from ". self::$instance->userTable . " where " . self::$instance->whereSql($fields,"AND") . "";
		$result = mssql_query($sql);
		if(!$result) self::$instance->errors[] = "ERROR: ".$sql;
		return $result;
	}
	
}