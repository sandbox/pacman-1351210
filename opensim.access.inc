<?php
/**
 * @file
 * opensim.access.inc
 * - Implemented by any database driver or web service or other connector that accesses typical
 *   functionality from the OpenSim system.
 */
interface openSimAccess {
	
	/**
	 * Get All User Data from Open Sim User Table
	 */
	static public function getOpenSimUserData($fields);
				
	/**
	 * Create an Open Sim User/Avatar
	 */
	static public function insertOpenSimAvatar($fields);
	
	/**
	 * Create an Open Sim User for the Grid
	 */
	static public function insertGridUser($fields);
	
	/**
	 * Create an Inventory Folder for Open Sim User
	 */
	static public function insertUserInventoryFolder($fields);
	
	/**
	 * Get the RegionId of an Open Sim User
	 */
	static public function getUserRegionId($fields);
	
	/**
	 * Get PricipalID of an Open Sim User given first and last name
	 */
	static public function getPrincipalIDGivenName($fields);
	
	/**
	 * Create an Open Sim User Auth Entry
	 */
	static public function insertUserAuth($fields);
	
	/**
	 * Update an Open Sim User Auth Entry
	 */
	static public function updateUserAuth($setFields,$updateFields);
	
}